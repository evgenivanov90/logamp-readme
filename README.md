#USED TECHNOLOGIES
1. Backend:
    * **yii php framework** for building api.
    * **MySQL** as database storage
    * **nginx** as web server
2. On frontend we are used **AngularJS**.

#REQUIREMENTS
1. PHP >= 5.4
2. FFMPEG: [Installation on Ubuntu](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu)  
 Cope all bin files to /usr/local/bin
3. wav2json [Website](http://beschulz.github.io/wav2json/)
3. Gearman: [Website](http://gearman.org/)  
 Installation:
* `sudo apt-get install gperf libmysqlclient-dev uuid-dev libevent-dev libcurl4-openssl-dev curl memcached libboost-all-dev make libtool flex bison autoconf gcc`   
* Download sources from [Launchpad](https://launchpad.net/gearmand/+download)
* Also install gearman from pecl extensions.
* `tar xzf gearmand-X.Y.tar.gz`
* `cd gearmand-X.Y`
* `./configure`
* `make`
* `make install`
   

# DEPLOYING
1. Get composer: `curl -sS https://getcomposer.org/installer | php`
2. Install vendors: `php composer.phar update`
3. Set up your web server.  
Config for **Nginx** and **Apache 2.4** you can find in configs/servers/. Notice on `**path to project dir**`.  
For **Apache** also you must enable `mod_proxy` and `mod_proxy_http` modules.
4. Check configuration for connecting to db. If you have different config params from `main.php` you MUST create `dev.php` with different parts of config. It will be merged to main config. Also this file won't be added to git.
5. Apply migrations for deploy database: `./yiic migrate`
7. Run  
`gearmand: gearmand -q mysql --mysql-host localhost --mysql-user root --mysql-db logamp -d -l path/to/gearman-logfile.log`
6. Run job workers (must be run from server user):  
     `sudo -u www-data ./yiic gearman_audio_processing`

# RULES
1. Changes in db only through yii migrations.
2. All code must be **PSR** compatible. About **PSR** your can read [here](http://www.php-fig.org/).
3. Thin controllers.
4. You MUST test `down` method in migrations. From this depends normal flow of rollback.
5. Names of migrations which add some data to DB must have prefix **"data_"**.
6. Migrations which change structure of DB must be separated from migrations which works with data, and their names must has prefix **"structure_"**.

# CLI COMMANDS
1. Default migrate commands, such like `migrate`, `migrate up [N]`, `migrate down [N]`. **Note** Restrictions you must set up manually.
2. `migrate_reverse_initial [dbName]` - Create migration file with current database structure. **Note:** must be used only once: as first migrate call.
3. [**Command in plans:**] `migrate_reverse_entity tableName entityId [dbName]` - Create migration file with entity record with all foreign records.

# API routes
1. Current api version: **v1**
2. Api base url: **/api/<version:\w+>/**
3. Api based on REST methodology.
3. Methods rules:  
**GET: /entity/** - request resolved to **actionGetAll** controller method. Returns array of entities.  
**GET: /entity/<id:\d+>** -  request resolved to **actionGet** controller method. Returns entity.  
**POST: /entity/** - request resolved to **actionCreate** controller method and must container JSON in POST data. Returns created entity.  
**PUT: /entity/<id:\d+>** - request resolved to **actionUpdate** controller method and updated entity with requested **id**.  
**DELETE: /entity/<id:\d+>** - request resolved to **actionDelete** controller method and deleted entity with requested **id**.

##NON-REST API CALLS
* **GET: /activate-account/<token:\w+>** - Used from *activation* emails.
* **GET: /reset-password/<token:\w+>** - Used from *reset password* emails.
* **POST: /uploads/<fileTypeId:\d+>** - Send 'form-data' form with files for upload them to service.  
#####Supported file types:
  * TYPE_AVATAR = 1;  
  * TYPE_IMAGES = 2;  
  * TYPE_AUDIO = 3;  
  * TYPE_TEXT = 4;
  * TYPE_JSON_WAVE = 5;  

##Countries
* **GET: /countries/** - Get list of all countries.

##Cities
* **GET: /cities/<id:\d+>** - Get city by its id.
* **GET: /cities/search/<countryId:\d+>/<name:[\w\s]+>** - Get list of cities from specified country and which names started from $name symbols.

##Complaints
* **POST: /complaints/** - Create complaint.

##Categories
* **GET: /categories/<entityTypeId:\d+>** - Get categories for specific entity type

##Lisenses
* **GET: /licenses/** - Get all licenses list
* **GET: /licenses/<id:\d+>** - Get license by id

##Layouts
* **GET: /layouts/** - Get all layouts

##Color Schemes 
* **GET: /colorSchemes/** - Get all color schemes

##Users

####Profiles
* **GET: /profiles/<userSlug:\w+>/** - Get user's profile by slug.

####Comments
* **GET: /comments/<entityTypeId:\d+>/<entityId:\d+>** - Get all comments for specific entity
* **GET: /comments/<entityTypeId:\d+>/<entityId:\d+>/<commentId:\d+>** - Get specific comment by their
* **POST: /comments/<entityTypeId:\d+>/<entityId:\d+>** - Create new comment for specific entity

####Settings
* **GET: /users/<userId:\d+>/settings** - Get user's settings.

####Bookmarks
* **GET: /users/<userId:\d+>/bookmarks/** - Get all user's bookmarks.
* **GET: /users/<userId:\d+>/bookmarks/<id:\d+>** - Get specific user's bookmark.
* **POST: /users/<userId:\d+>/bookmarks/** - Create new user's bookmark.
* **DELETE: /users/<userId:\d+>/bookmarks/<id:\d+>** - Delete specific user's bookmark.

####Shares
* **GET: /users/<userId:\d+>/shares/** - Get all user's shares.
* **GET: /users/<userId:\d+>/shares/<id:\d+>** - Get specific user's share.
* **POST: /users/<userId:\d+>/shares/** - Create new user's share.
* **DELETE: /users/<userId:\d+>/shares/<id:\d+>** - Delete specific user's share.

####Followings
* **GET: /users/<userId:\d+>/followings/** - Get all user's followings.
* **GET: /users/<userId:\d+>/followings/<id:\d+>** - Get specific user's following.
* **POST: /users/<userId:\d+>/followings/** - Create new user's following.
* **DELETE: /users/<userId:\d+>/followings/<id:\d+>** - Delete specific user's following.

####Followers
* **GET: /users/<userId:\d+>/followers/** - Get all user's followers.
* **GET: /users/<userId:\d+>/followers/<id:\d+>** - Get specific user's follower.
* **DELETE: /users/<userId:\d+>/followers/<id:\d+>** - Delete specific follower.

####Playlists
* **GET: /users/<userId:\d+>/playlists/** - Get all user's playlists.
* **GET: /users/<userId:\d+>/playlists/<playlistId:\d+>/copy** - Copy playlist to current user.
* **POST: /users/<userId:\d+>/playlists/** - Create new playlist.
* **PUT: /users/<userId:\d+>/playlists/<playlistId:\d+>** - Update playlist's data.
* **DELETE: /users/<userId:\d+>/playlists/<playlistId:\d+>** - Delete user's playlist with all attached songs.


* **GET: /users/<userId:\d+>/playlists/<playlistId:\d+>/songs** - Get all user's playlist songs.
* **GET: /users/<userId:\d+>/playlists/<playlistId:\d+>/songs/<songId:\d+>** - Get specific song data from playlist.
* **POST: /users/<userId:\d+>/playlists/<playlistId:\d+>/songs/** - Attach new song to playlist.
* **PUT: /users/<userId:\d+>/playlists/<playlistId:\d+>/songs/<songId:\d+>** - Edit song's data.
* **DELETE: /users/<userId:\d+>/playlists/<playlistId:\d+>/songs/<songId:\d+>** - Delete song from playlist.

##Entities
* **GET: /users/<userId:\d+>/entities/last** - Get last entities for profile
* **GET: /users/<userId:\d+>/entities/<entityTypeId:\d+>** - Get all entities by type id
* **GET: /users/<userId:\d+>/entities/<entityTypeId:\d+>/<entityId:\d+>** - Get specific entity record.
* **POST /users/<userId:\d+>/entities/** - Create new entity record
* **PUT /users/<userId:\d+>/entities/<entityTypeId:\d+>/<entityId:\d+>** - Update specific entity record.
* **DELETE /users/<userId:\d+>/entities/<entityTypeId:\d+>/<entityId:\d+>** - Delete specific entity record.

####Likes
* **GET: /entities/<entityTypeId:\d+>/<entityId:\d+>/likes/** - Get all entity's likes.
* **GET: /entities/<entityTypeId:\d+>/<entityId:\d+>/likes/<id:\d+>** - Get specific entity's like.
* **POST: /entities/<entityTypeId:\d+>/<entityId:\d+>/likes/** - Create new like.
* **DELETE: /entities/<entityTypeId:\d+>/<entityId:\d+>/likes/<id:\d+>** - Delete like.

